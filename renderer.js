const { ipcRenderer } = require('electron');

const { insertData, loadData, deleteRow, update } = require('./db.js');
const teamDB = require('./db/team/teamDB.js');
var electron = require('electron');
let dbName = electron.remote.getGlobal('dbName'); 
let ADO = electron.remote.getGlobal('ADO');

/* document.querySelector('#save').addEventListener('click', (event) => {
    event.preventDefault();
    let name_input = document.getElementById('name');
    let lastname_input = document.getElementById('lastname');
    let age_input = document.getElementById('age');
    let id_input = document.getElementById('id');   
    
    if(name_input.value !== '' && lastname_input.value !== '' && age_input.value !== '')
    { 
        if(id_input.value !== '')
        {
            console.log('actualizar elementos');
            update(id_input.value, name_input.value, lastname_input.value, age_input.value).then(data => {
                // document.getElementById('formdata').reset();
                setInputValue('', '', '', '');
                showData();             
            });
        }else{
            insertData(name_input.value, lastname_input.value, age_input.value).then(data => {
                document.getElementById('formdata').reset()    
                showData();
            });
        }
    }
    
}); */

/* document.querySelector('#open').addEventListener('click', function (event) {
    dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [
            
            { name: 'Custom File Type', extensions: ['as'] },
            { name: 'All Files', extensions: ['*'] }
          ]
    }).then( files => {
        console.log(files.filePaths[0]);
        // Connect to the MS Access DB
        if(files.filePaths[0] !== undefined){
            openDB(files.filePaths[0]);
        }
             
    }).catch(err => {
        console.log(err)
      });
});
 */

const showData = () => {
    teamDB.loadData(ADO, dbName).then(datos => {
        let tbody = document.getElementById('tbody');
        tbody.innerHTML = '';
        datos.forEach(element => {
            var tr = document.createElement("tr");
    
            var td = document.createElement("td");
            var text = document.createTextNode(element.name);
            td.appendChild(text);
            tr.appendChild(td);
    
            td = document.createElement("td");
            text = document.createTextNode(element.color);
            td.appendChild(text);
            tr.appendChild(td);    

            td = document.createElement("td");
            var deleteBTN = document.createElement("button");
            text = document.createTextNode("DELETE");
            deleteBTN.appendChild(text);
            deleteBTN.setAttribute("class", "btn btn-danger");
            deleteBTN.setAttribute("id", element.id);
            deleteBTN.addEventListener("click" , (e) => {
                console.log("eliminar ", e.target);
                console.log(e.target.id);
                teamDB.del(ADO, dbName, e.target.id).then(data => {
                    console.log('elemento elimindo ok');
                    showData();
                });
            });
            td.appendChild(deleteBTN);
            // td.appendChild(text);
            tr.appendChild(td);

            td = document.createElement("td");
            var editBTN = document.createElement("button");
            text = document.createTextNode("EDIT");
            editBTN.appendChild(text);
            editBTN.setAttribute("class", "btn btn-primary");
            editBTN.addEventListener('click', (e) => {
                // console.log(element.name, element.last_name, element.age);
                // setInputValue(element.name, element.last_name, element.age, element.id);
                ipcRenderer.send('edit-team-data', {
                    /* name: element.name,
                    color: element.color, */
                    id: element.id
                });
            });

            td.appendChild(editBTN);
            tr.appendChild(td);
    
            tbody.appendChild(tr);
        });
       // document.getElementById('tableData').appendChild(tbody);
    }).catch(err => {
        console.log('error', err);
    });

    // ipcRenderer.send('data-send', {msg: 'datos enviados'});
}

ipcRenderer.on('actualizar', () =>{
    dbName = electron.remote.getGlobal('dbName');
    ADO = electron.remote.getGlobal('ADO');
    console.log('actualizando interfaz', dbName);
    showData();
});

// showData();
console.log(dbName);