'use strict';
/* if (process.mainModule.filename.indexOf('app.asar') !== -1) {
    // In that case we need to set the correct path to adodb.js
    ADODB.PATH = './resources/adodb.js';
  }
 */  

//var ADODB = require('node-adodb');
//const ADODB = require('electron').remote.getGlobal('ADO')

//ADODB.debug = true;


let connection;

const openDB = (ADODB,dbName) =>{
    
    connection = ADODB.open(`Provider=Microsoft.Jet.OLEDB.4.0;Data Source=${dbName};`);    
}

const close = () =>{
    connection.close();
}

const loadData = async (ADODB, dbName) =>{
    openDB(ADODB, dbName);
    if(connection)
    {
        let data = await connection
        .query('SELECT * FROM [team];'); 
        return data
    }
    close();
    return Promise.reject(Error("DB close"));
}

const insert = async (ADODB, dbName,name, color ) =>{
    openDB(ADODB, dbName);
    if(connection){
        let query = `INSERT INTO [team](name, color) VALUES ("${name}", "${color}")`;
        // console.log('query ', query);
        // connection = ADODB.open(`Provider=Microsoft.ACE.OLEDB.15.0;Data Source=db.accdb;Persist Security Info=False;`);        
        let result = connection
        .execute(query);
        
        return result;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const update = async (ADODB, dbName, id, name, color) =>{
    openDB(ADODB, dbName);
    if(connection)
    {
        let query = `UPDATE [team] SET name = "${name}", color = "${color}" WHERE id = ${id}`;
        return connection.execute(query);
    }
    close();
    return Promise.reject(Error("DB close"));
}

const del = async (ADODB, dbName,id) => {
    openDB(ADODB,dbName);
    if(connection){
        let query = `DELETE * FROM [team] WHERE id=${id}`;
        let result = connection.execute(query)
        return result;
    }
    close();
    return Promise.reject(Error("DB close"));
}

const get = async (ADODB, dbName, id) =>{
    openDB(ADODB, dbName);
    if(connection)
    {
        let data = await connection
        .query(`SELECT * FROM [team] WHERE id=${id};`); 
        return data
    }
    close();
    return Promise.reject(Error("DB close"));
}

//openDB('app-prueba.mdb');
module.exports = { loadData, insert, update, del, get };