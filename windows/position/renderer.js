const { ipcRenderer, ipcMain } = require('electron');
const positionDB = require('../../db/position/positionDB.js');
var electron = require('electron');
let dbName = electron.remote.getGlobal('dbName'); 
let ADO = electron.remote.getGlobal('ADO');

const showAll = () => {
    positionDB.loadData(ADO, dbName).then(d => {
    let tbody = document.getElementsByTagName('tbody')[0];
    tbody.innerHTML = '';

    d.forEach(e => {
        var tr = document.createElement("tr");
    
        var td = document.createElement("td");
        var text = document.createTextNode(e.position_name);
        td.appendChild(text);
        tr.appendChild(td);

        td = document.createElement("td");
        text = document.createTextNode(e.description);
        td.appendChild(text);
        tr.appendChild(td);

        td = document.createElement("td");
        var deleteBTN = document.createElement("button");
        text = document.createTextNode("DELETE");
        deleteBTN.appendChild(text);
        deleteBTN.setAttribute("class", "btn btn-danger");
        deleteBTN.setAttribute("id", e.id);
        deleteBTN.addEventListener("click" , (event) => {
            console.log("eliminar ", event.target);
            console.log(event.target.id);
            positionDB.del( ADO, dbName, event.target.id).then(data => {
                console.log('elemento elimindo ok');
                showAll();
            });
        });
        td.appendChild(deleteBTN);
        // td.appendChild(text);
        tr.appendChild(td);

        td = document.createElement("td");
        var editBTN = document.createElement("button");
        text = document.createTextNode("EDIT");
        editBTN.appendChild(text);
        editBTN.setAttribute("class", "btn btn-primary");
        editBTN.addEventListener('click', (event) => {
            // console.log(element.name, element.last_name, element.age);
            // setInputValue(element.name, element.last_name, element.age, element.id);
            ipcRenderer.send('edit-position-data', {
                /* name: element.name,
                color: element.color, */
                id: e.id
            });
        });

        td.appendChild(editBTN);
        tr.appendChild(td);

        tbody.appendChild(tr);
    });
    }).catch(err => console.log('error', err));
};

document.getElementById('close').addEventListener('click', () =>{
    ipcRenderer.send('close-position-new', { close: true});
});

let save_button = document.getElementById('save');
if(save_button){
    save_button.addEventListener('click', (event)=> {
        let id_input = document.getElementById('id');
        let name_input = document.getElementById('name');
        let description_input = document.getElementById('description');

        ipcRenderer.send('position-data-send', {
            id: id_input.value,
            name: name_input.value,
            description: description_input.value
        });
        ipcRenderer.send('close-position-new', { close: true});
    });
}

ipcRenderer.on('load-data-position', (e, data) => {
    showAll();
});

ipcRenderer.on('edit-data-view', (e, data) => {
    console.log('datos recividos para la edicion ', data);
    let id_input = document.getElementById('id');
    let name_input = document.getElementById('name');
    let description_input = document.getElementById('description');

    id_input.value = data.id;
    name_input.value = data.position_name;
    description_input.value = data.description;
});

ipcRenderer.on('title', (e, title) =>{
    document.getElementById('title').innerText = title;
    document.getElementsByTagName('title')[0].innerText = title;
});

ipcRenderer.on('actualizar', () =>{
    dbName = electron.remote.getGlobal('dbName');
    ADO = electron.remote.getGlobal('ADO');
    console.log('actualizando interfaz', dbName);
    
});