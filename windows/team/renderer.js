const { ipcRenderer } = require('electron');

document.getElementById('save').addEventListener('click', (event)=>{
    event.preventDefault();

    let name_input = document.getElementById('name');
    let color_input = document.getElementById('color');
    let id_input = document.getElementById('id');

    console.log('enviar datos');
    ipcRenderer.send('team-data-send', {
        name: name_input.value,
        color: color_input.value,
        id: id_input.value
    });
    ipcRenderer.send('close-team-new', { close: true});
});

document.getElementById('close').addEventListener('click', () => {
    ipcRenderer.send('close-team-new', { close: true});
});

ipcRenderer.on('edit-data-view', (e, data) =>{
    console.log('reciviendo datos ', data)
    let name_input = document.getElementById('name');
    let color_input = document.getElementById('color');
    let id_input = document.getElementById('id');

    id_input.value = data.id;
    name_input.value = data.name;
    color_input.value = data.color;

});

ipcRenderer.on('title', (e, title) =>{
    document.getElementById('title').innerHTML = title;
    document.getElementsByTagName('title')[0].innerHTML = title;
})