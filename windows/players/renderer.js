const { ipcRenderer, ipcMain } = require('electron');
const playerDB = require('../../db/player/playerDB.js');
var electron = require('electron');
let dbName = electron.remote.getGlobal('dbName');
let ADO = electron.remote.getGlobal('ADO');

const showAll = () =>{
    playerDB.loadData(ADO, dbName).then(d =>{
        console.log('datos devueltos', d);
        let tbody = document.getElementsByTagName('tbody')[0];
        tbody.innerHTML = '';

        d.forEach(e => {
            var tr = document.createElement("tr");
    
            var td = document.createElement("td");
            var text = document.createTextNode(e.firstname);
            td.appendChild(text);
            tr.appendChild(td);
    
            td = document.createElement("td");
            text = document.createTextNode(e.lastname);
            td.appendChild(text);
            tr.appendChild(td);

            td = document.createElement("td");
            text = document.createTextNode(e.alias);
            td.appendChild(text);
            tr.appendChild(td);

            td = document.createElement("td");
            text = document.createTextNode(e.age);
            td.appendChild(text);
            tr.appendChild(td);

            td = document.createElement("td");
            text = document.createTextNode(e.weigh);
            td.appendChild(text);
            tr.appendChild(td);

            td = document.createElement("td");
            text = document.createTextNode(e.carve);
            td.appendChild(text);
            tr.appendChild(td);

            td = document.createElement("td");
            var deleteBTN = document.createElement("button");
            text = document.createTextNode("DELETE");
            deleteBTN.appendChild(text);
            deleteBTN.setAttribute("class", "btn btn-danger");
            deleteBTN.setAttribute("id", e.id);
            deleteBTN.addEventListener("click" , (event) => {
                console.log("eliminar ", event.target);
                console.log(event.target.id);
                playerDB.del(ADO, dbName, event.target.id).then(data => {
                    console.log('elemento elimindo ok');
                    showAll();
                });
            });
            td.appendChild(deleteBTN);
            // td.appendChild(text);
            tr.appendChild(td);

            td = document.createElement("td");
            var editBTN = document.createElement("button");
            text = document.createTextNode("EDIT");
            editBTN.appendChild(text);
            editBTN.setAttribute("class", "btn btn-primary");
            editBTN.addEventListener('click', (event) => {
                // console.log(element.name, element.last_name, element.age);
                // setInputValue(element.name, element.last_name, element.age, element.id);
                ipcRenderer.send('edit-player-data', {
                    /* name: element.name,
                    color: element.color, */
                    id: e.id
                });
            });

            td.appendChild(editBTN);
            tr.appendChild(td);
    
            tbody.appendChild(tr);
        });
    }).catch(err => console.log(err));
};

let edit_button = document.getElementById('save') 
if(edit_button){
    edit_button.addEventListener('click', (e) => {
        e.preventDefault();
        let id_input = document.getElementById('id');
        let name_input = document.getElementById('name');
        let lastname_input = document.getElementById('lastname');
        let alias_input = document.getElementById('alias');
        let age_input = document.getElementById('age');
        let weigh_input = document.getElementById('weigh');
        let carve_input = document.getElementById('carve');
    
        ipcRenderer.send('player-data-send', {
            name: name_input.value,
            lastname: lastname_input.value,
            alias: alias_input.value,
            age: age_input.value,
            weigh: weigh_input.value,
            carve: carve_input.value,
            id: id_input.value
        });
        ipcRenderer.send('close-player-new', { close: true});
    });
}

document.getElementById('close').addEventListener('click', () =>{
    ipcRenderer.send('close-player-list', { close: true});
});

ipcRenderer.on('load-data-player', (e) => {
    console.log('mostrar datos players')
    showAll();
});

ipcRenderer.on('edit-data-view', (e, data) => {
    console.log('datos a editar ', data);
    let id_input = document.getElementById('id');
    let name_input = document.getElementById('name');
    let lastname_input = document.getElementById('lastname');
    let alias_input = document.getElementById('alias');
    let age_input = document.getElementById('age');
    let weigh_input = document.getElementById('weigh');
    let carve_input = document.getElementById('carve');

    id_input.value = data.id;
    name_input.value = data.firstname;
    lastname_input.value = data.lastname;
    age_input.value = data.age;
    weigh_input.value = data.weigh;
    carve_input.value = data.carve;
});

ipcRenderer.on('title', (e, title) => {
    console.log('title ', title);
    document.getElementById('title').innerHTML = title;
    document.getElementsByTagName('title')[0].innerHTML = title;
});

ipcRenderer.on('actualizar', () =>{
    dbName = electron.remote.getGlobal('dbName');
    ADO = electron.remote.getGlobal('ADO');
    console.log('actualizando interfaz', dbName);
    //showData();
});