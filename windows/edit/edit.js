const { ipcRenderer } = require('electron');



const setInputValue = (name, lastname, age, id) =>  {
    let nameInput = document.getElementById('name');
    let lastnameInput = document.getElementById('lastname');
    let ageInput = document.getElementById('age');
    let idInput = document.getElementById('id');

    nameInput.value = name;
    lastnameInput.value = lastname;
    ageInput.value= age;
    idInput.value = id;
};

document.getElementById('save').addEventListener('click', (event)=>{
    event.preventDefault();

    let name_input = document.getElementById('name');
    let lastname_input = document.getElementById('lastname');
    let age_input = document.getElementById('age');
    let id_input = document.getElementById('id');
    console.log('enviar datos');
    ipcRenderer.send('data-send', {
        name: name_input.value,
        lastname: lastname_input.value,
        age: age_input.value,
        id: id_input.value
    });
    ipcRenderer.send('close-edit', { close: true});
});

document.getElementById('close').addEventListener('click', () => {
    ipcRenderer.send('close-edit', { close: true});
});

ipcRenderer.on('edit-data-view', (e, data) => {
    console.log('datos que llegan', data);
    setInputValue(data.name, data.lastname, data.age, data.id);
});

ipcRenderer.on('title', (e, title) => {
    document.getElementById('title').innerHTML = title;
    document.getElementsByTagName('title')[0].innerHTML = title;
});
