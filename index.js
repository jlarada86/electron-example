var ADODB = require('node-adodb');
if (process.mainModule.filename.indexOf('app.asar') !== -1) {
  // In that case we need to set the correct path to adodb.js
  ADODB.PATH = './resources/adodb.js';
}
ADODB.debug = true;
// para usar en el acceso a datos
global.ADO = ADODB;
console.log('ADO',global.ADO);

const { app, BrowserWindow, ipcMain, Menu, dialog } = require('electron');
const path = require('path');
const { insertData, loadData, deleteRow, update } = require('./db.js');
const teamDB = require('./db/team/teamDB.js');
const playerDB = require('./db/player/playerDB.js');
const positionDB = require('./db/position/positionDB.js');

const nodeEnv = process.env.NODE_ENV;
global.dbName = '';

/* if(nodeEnv === 'development'){
  global.ADODB = require('node-adodb')
} else {
  let libPath = path.join(app.getAppPath(), 'node_modules/node-adodb').replace('app.asar', 'app.asar.unpacked')
  global.ADODB = require(libPath)
} */

let win;
let editW;
let winTeamNew;
let winPlayerNew;
let winListPlayer;
let winPositionNew;
let winPositionList;

const menuTemplate = [
  {
    label: 'General',
    submenu: [
      {
        label: 'New Position',
        click(){
          if(dbName === '')
          {
            showMsgOpenDB();
          }else{
            newPositionWindow();
          }
          
        }
      },
      {
        label: 'List Position',
        click(){
          if(dbName === '')
          {
            showMsgOpenDB();
          }else{
            newListPositionWindow();
          }          
        }
      },
      { type: 'separator'},
      {
        label: 'Open DB',
        click(){
          OpenDB();
        }
      },
      { type: 'separator' },
      {
        label: 'Exit',
        accelerator: 'Crtl-q',
        click(){
          console.log('salir');
          app.quit();
        }
      }
    ]
  },
  {
    label: 'Team manager',
    submenu: [
      {
        label: "New team",
        click(){
          if(dbName === '')
          {
            showMsgOpenDB();
          }else{
            newTeamWindow();
          }
          
        }
      }
    ]
  },
  {
    label: 'Players manager',
    submenu: [
      {
        label: "New player",
        click(){
          if(dbName === '')
          {
            showMsgOpenDB();
          }else{
            newPlayerWindow();
          }
        }
      },
      {
        label: "List Players",
        click(){
          if(dbName === '')
          {
            showMsgOpenDB();
          }else{
            newListPlayerWindow();
          }
        }
      }
    ]
  }
];

function showMsgOpenDB()
{
  const options = {              
    title: 'Info',
    message: 'Please open db first'             
  };
  dialog.showMessageBox(null, options);
}

function OpenDB(){
  dialog.showOpenDialog({
    properties: ['openFile'],
    filters: [
        
        { name: 'Custom File Type', extensions: ['mdb'] },
        { name: 'All Files', extensions: ['*'] }
      ]
    }).then( files => {
        console.log(files.filePaths[0]);
        // Connect to the MS Access DB
        if(files.filePaths[0] !== undefined){
            //openDB(files.filePaths[0]);
            console.log('fichero seleccionado ', files.filePaths[0]);
            global.dbName = files.filePaths[0];
            win.webContents.send('actualizar', {});
        }
            
    }).catch(err => {
        console.log(err)
      });
}

function newTeamWindow(){
  winTeamNew = new BrowserWindow({
    title: 'new team',
    width: 450,
    height: 600,
    parent: win,
    //modal: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  winTeamNew.loadFile('windows/team/new.html');
 // winTeamNew.setMenu(null);
  //editW.show();
}

function newPositionWindow(){
  winPositionNew = new BrowserWindow({
    title: 'new team',
    width: 450,
    height: 600,
    parent: win,
    //modal: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  winPositionNew.loadFile('windows/position/new.html');
 // winTeamNew.setMenu(null);
  //editW.show();
}

function newListPositionWindow(){
  winPositionList = new BrowserWindow({
    title: 'new team',
    width: 450,
    height: 600,
    parent: win,
    //modal: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  winPositionList.loadFile('windows/position/list.html');
  winPositionList.webContents.on('did-finish-load', () => {
    console.log('ventana lista list');
    winPositionList.webContents.send('load-data-position', true);
    
  });
}

function newListPlayerWindow(){
  winListPlayer = new BrowserWindow({
    title: 'new team',
    width: 800,
    height: 600,
    parent: win,
    //modal: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  winListPlayer.loadFile('windows/players/list.html');
  winListPlayer.webContents.on('did-finish-load', () => {
    console.log('ventana lista list');
    winListPlayer.webContents.send('load-data-player', true);
    
  });
 // winTeamNew.setMenu(null);
  //editW.show();
}

function newPlayerWindow(){
  winPlayerNew = new BrowserWindow({
    title: 'new player',
    width: 450,
    height: 700,
    parent: win,
    //modal: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  winPlayerNew.loadFile('windows/players/new.html');
 
}


/* function editWindow(){
  editW = new BrowserWindow({
    title: 'Edit',
    width: 450,
    height: 600,
    parent: win,
    //modal: true,
    webPreferences: {
      nodeIntegration: true
    }
  });

  editW.loadFile('windows/edit/edit.html');
  //editW.show();
} */

function createWindow () {
    
  // Create the browser window.
  win = new BrowserWindow({
    width: 800,
    height: 600,
    //frame: false,
    webPreferences: {
      nodeIntegration: true
    }
  })

  // and load the index.html of the app.
  win.loadFile('index.html');
  win.maximize();
  // Open the DevTools.
  //win.webContents.openDevTools();

  const mainMenu = Menu.buildFromTemplate(menuTemplate);
  Menu.setApplicationMenu(mainMenu);
}


// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow)

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow()
  }
})

app.on('ready', ()=>{
   
});


// team
ipcMain.on('edit-team-data',(e, data) =>{
  console.log('actualizar team');
  newTeamWindow();
  winTeamNew.webContents.on('did-finish-load', () => {
    console.log('ventana lista');
    console.log('id a buscar ', data.id, global.dbName );
    teamDB.get(ADODB, global.dbName , data.id).then(d => {
      
      winTeamNew.webContents.send('edit-data-view', d[0]);
      winTeamNew.webContents.send('title', 'Editing');
    })
    
  });
  
});

ipcMain.on('team-data-send', (e, data) => {
  console.log('datos llegados esde new team ', data);
  if(data.id !== ''){
    teamDB.update(ADODB, global.dbName, data.id, data.name, data.color).then(( ) =>{
      console.log('datos actualizados');
      win.webContents.send('actualizar', {});
    }).catch(err => {
      console.log('error ', err);
    })
  }else{
    teamDB.insert(ADODB, global.dbName, data.name, data.color).then( () => {
      console.log('datos guardados correctamente');
      win.webContents.send('actualizar', {});
    }).catch(err => {
      console.log('error ', err);
    });
  }
});

ipcMain.on('close-team-new', (e) =>{
  winTeamNew.destroy();
  console.log('cerrando new team windows')
})
// ============

// players
ipcMain.on('close-player-new', (e) => {
  winPlayerNew.destroy();
  console.log('ventana cerrada');
});

ipcMain.on('close-player-list', (e) => {
  if(winListPlayer)
    winListPlayer.destroy();
  if(winPlayerNew)
    winPlayerNew.destroy();
  console.log('ventana cerrada');
});

ipcMain.on('edit-player-data', (e, data) => {
  playerDB.get(ADODB, global.dbName , data.id).then(d => {
    newPlayerWindow();
    winPlayerNew.webContents.on('did-finish-load', () => {
      console.log('ventana lista');
      winPlayerNew.webContents.send('edit-data-view', d[0]);
      winPlayerNew.webContents.send('title', 'Editing');
      winListPlayer.destroy();
    });
  }).catch(err => console.log('error ', err));
});

ipcMain.on('player-data-send', (e, data) =>{
  console.log('datos del player ', data);
  if(data.id !== ''){
    playerDB.update(ADODB, global.dbName, data.id, data.name, data.lastname, data.alias, data.age, data.weigh, data.carve).then(() =>{
      console.log('datos actualizados');
    }).catch(err => {
      console.log('error ', err);
    });
  }else{
    playerDB.insert(ADODB, global.dbName, data.name, data.lastname, data.alias, data.age, data.weigh, data.carve).then(() =>{
      console.log('datos insertados');
    }).catch(err => {
      console.log('error ', err);
    });
  }
  newListPlayerWindow();
  winListPlayer.webContents.on('did-finish-load', () => {
    winListPlayer.webContents.send('load-data-player', true);
    winPlayerNew.destroy();
  });

});
// ==============

// POSITION
ipcMain.on('close-position-new', (e)=>{
  if(winPositionNew)
    winPositionNew.destroy();
  if(winPositionList)
    winPositionList.destroy();
});

ipcMain.on('position-data-send', (e, data)=> {
  console.log('datos de la posision', data);
  if(data.id !== ''){
    positionDB.update(ADODB, global.dbName, data.id, data.name, data.description).then(d =>{
      console.log('datos actualizados');
    }).catch(err => console.log('error update', err));
  }else{
    positionDB.insert(ADODB, global.dbName, data.name, data.description).then(d => {
      console.log('datos insertados')
    }).catch(err => console.log('error insert', err));
  }
});

ipcMain.on('edit-position-data', (e, data) => {
  positionDB.get(ADODB, global.dbName, data.id).then( d => {
    newPositionWindow();
    winPositionNew.webContents.on('did-finish-load', () => {
      console.log('ventana lista');
      winPositionNew.webContents.send('edit-data-view', d[0]);
      winPositionNew.webContents.send('title', 'Editing');
      winPositionList.destroy();
    });
    
  }).catch(err => console.log('error', err))
  winPositionList.destroy();
});
// =============

if(process.env.NODE_ENV !== 'production'){
  menuTemplate.push({
    label: 'dev Tools',
    submenu: [
      {
        label: 'Show/Hide devtools',
        click(item, focusedWindow){
          focusedWindow.toggleDevTools();
        }
      },
      {
        role: 'reload'
      }
    ]
  });
}
// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.