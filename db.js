'use strict';
/* if (process.mainModule.filename.indexOf('app.asar') !== -1) {
    // In that case we need to set the correct path to adodb.js
    ADODB.PATH = './resources/adodb.js';
  }
 */  
var ADODB = require('node-adodb');
//const ADODB = require('electron').remote.getGlobal('ADODB')

ADODB.debug = true;


let connection;

const openDB = (fileName) =>{
    connection = ADODB.open(`Provider=Microsoft.Jet.OLEDB.4.0;Data Source=${fileName};`);    
}

const loadData = async () =>{
    // connection = ADODB.open(`Provider=Microsoft.ACE.OLEDB.15.0;Data Source=db.accdb;Persist Security Info=False;`);
    // console.log('objecto de la consulta',connection);
    // Query the DB
    /* connection
        .query('SELECT * FROM [datos];').then(data => console.log(data));  */

    let data = await connection
    .query('SELECT * FROM [datos];'); 
    return data
}

const insertData = (name, lastname, age) => {
    
    let query = `INSERT INTO [datos](name,last_name, age) VALUES ("${name}", "${lastname}", ${age})`;
    // console.log('query ', query);
    // connection = ADODB.open(`Provider=Microsoft.ACE.OLEDB.15.0;Data Source=db.accdb;Persist Security Info=False;`);        
    let result = connection
    .execute(query);
    
    return result;
}

const update = (id, name, lastname, age) =>{
    let query = `UPDATE [datos] SET name = "${name}", last_name = "${lastname}", age = ${age} WHERE id = ${id}`;
    return connection.execute(query);
}

const deleteRow = (id) => {
    let query = `DELETE * FROM [datos] WHERE id=${id}`;
    let result = connection.execute(query)
    return result;
}

openDB('node-adodb.mdb');
module.exports = { insertData, loadData, deleteRow, update };